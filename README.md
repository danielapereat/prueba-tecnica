# ¡Hola!

Para nosotros es genial que solicites unirte a nuestro equipo de desarrollo y diseño de producto. Para tener una idea de tu trabajo, nos gustaría que completes el siguiente test. Teniendo en mente que no hay un tiempo mínimo para entregarlo, tienes un periodo máximo de 4-5 días para terminarlo una vez iniciado el test.

## Requerimientos esenciales

- El test debe ser 100% Responsive
- Usar **React** cómo librería FE
- Uso de **React Hooks**
- Usar administrador de paquetes (**yarn, npm**) para el manejo de dependencias
- Buen manejo del controlador de versiones **git**

## Situación

Se solicita diseño y creación a criterio del(a) desarrollador(a) de una vista que renderice una sola lista que integre: **depósitos, retiros e intercambios** ordenados **cronológicamente**. Cada elemento de la lista debe ir representado a través de un **“card”** con la información que el(a) desarrollador(a) considere relevante para el usuario, para esto debe:

- **1.**`Crear entorno de desarrollo (WebPack | CRA)`
- **2.**`Integrar React y Redux`
- **3.**`Crear webService para el manejo de peticiones HTTP`
- **4.**`Consultar y normalizar en el estado de redux las listas obtenidas` (sugerencia: puedes utilizar `normalizr` o la librería que gustes)
- **5.**`Renderizar lista de ordenes ordenada crónologicamente`
- **6.**`Crear detalle de orden` *(opcional)*
- **7.**`Implementar scroll infinito con un loader a la lista renderizada`
- **8.**`Implementar animación a los nuevos elementos agregados a la lista` *(opcional)*
- **9.**`Implementar manejo de errores. ej. Manejo de error al caducar el Token`

## Detalles

En las siguientes URL podrás consultar las listas descritas previamente (**depósitos, retiros e intercambios**), es necesario un `Token de autenticación` para consultar estas listas.

Variables:
> USER_ID = `60b1da870982680064b1bf04`

- **1.** Url APIS:
-   - *a.* **Depositos:** `https://deposit.bitsenda.com/api/users/${USER_ID}/deposits`
-   - *b.* **Retiros:** `https://withdraw.bitsenda.com/api/users/${USER_ID}/withdraws`
-   - *c.* **Intercambios:** `https://swap.bitsenda.com/api/users/${USER_ID}/swaps`

**Recursos:**
- > `Ver guía para obtener el Token de autenticación:` https://gitlab.com/andresarav/technical-test/-/blob/master/GET_TOKEN_GUIDE.md
- > `Ver proyecto de prueba con petición al API:` https://gitlab.com/andresarav/technical-test/-/tree/master/test-app

## Puntos extra

Estos puntos solo se tendrán en cuenta si las funcionalidades de los puntos anteriores fueron completadas correctamente

- **1.**`Utilice styled-components para el manejo de estilos`
- **2.**`Integre estrategia de optimización web` *(opcional)*
- **3.**`Agregue soporte y compatibilidad con navegadores`
- **4.**`Integre unit testing a funciones, hooks y métodos dentro de los componentes`

## Entrega

- **1.**`Agregue un README con instrucciones para arrancar el proyecto y con otra información donde argumente la estrategia de optimización web que utilizó (ej. auditoría lighthouse => code split, lazy loader,migración de recursos a CDN etc...)`

- **2.**`Cree una rama con su nombre y cree un Merge Request a la rama master del repositorio.`

Eso es todo, te deseamos suerte con tu prueba técnica, para resolver dudas e inquietudes escribe a soporte@coinsenda.com con el asunto "Desarrollo de Prueba técnica"
