import { useEffect } from 'react'
import Header from './components/Header'
//import Footer from './components/Footer'
import Content from './components/Content'
import styled from 'styled-components'
import {
  BrowserRouter as Router,
  Switch,
  Route,
 } from "react-router-dom";
// import {
//   AUTH_TOKEN,
//   USER_ID,
//   DEPOSIT_URL
//  } from './const'

 //Redux
 import {Provider} from 'react-redux';
 import store from './store';

function App() {

  // const handleError = err => {
  //   // TODO: Create a method to handle error
  //   console.log(err)
  //   return
  // }

  // const doFetch = async(url) => {
  //   try {
  //     const response = await fetch(`${DEPOSIT_URL}users/${USER_ID}/deposits`, {
  //       method: `GET`,
  //       headers:{
  //         Authorization: `Bearer ${AUTH_TOKEN}`,
  //       }
  //     });
  //     const finalResponse = await response.json();
  //     console.log(await finalResponse)
  //     return await finalResponse;
  //   } catch (_) {
  //     return handleError(_)
  //   }
  // }

 
  useEffect(() => {
    //doFetch()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  return (
    
    <Router>
      <Provider store = {store}>
        <Header/>
        <div className="container">
          <Switch>
            <Route exact path="/" component={Content}/>
          </Switch>
        </div>       
      </Provider> 
    </Router>
    
    
  );
}

export default App;
