import { schema } from 'normalizr';


export const idSchema = new schema.Entity('id');
export const userListSchema = new schema.Array(idSchema);