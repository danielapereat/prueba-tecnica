import {
    TRANSACTION_LOADING,
    TRANSACTION_SUCCESS,
    TRANSACTION_ERROR
} from '../const'
   
const initialState = {
    transacciones: [],
    error: null,
    loading: null, 
}

export default function(state = initialState, action){
    
    switch(action.type){
        case TRANSACTION_LOADING:
            return{
                ...state,
                loading: true,
                error: false
            }
        case TRANSACTION_SUCCESS:
            return{
                ...state,
                loading: false,
                transacciones: action.payload,
                error: false
            }
        case TRANSACTION_ERROR:
            return{
                ...state,
                loading: false,
                error: action.payload
            }
        default:
            return state;
    }
}