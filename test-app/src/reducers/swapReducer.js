import {
    SWAP_SUCCESS,
    SWAP_LOADING,
    SWAP_ERROR
} from '../const'
   
const initialState = {
    intercambios: [],
    error: null,
    loading: false, 
}

export default function(state = initialState, action){
    switch(action.type){
        case SWAP_LOADING:
            return{
                ...state,
                loading: true,
                error: false
            }
        case SWAP_SUCCESS:
            return{
                ...state,
                loading: false,
                intercambios: action.payload,
                error: false
            }
        case SWAP_ERROR:
            return{
                ...state,
                loading: false,
                error: action.payload
            }
        default:
            return state;
    }
}