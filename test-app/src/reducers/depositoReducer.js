import {
    DEPOSIT_SUCCESS,
    DEPOSIT_LOADING,
    DEPOSIT_ERROR
} from '../const'
   
const initialState = {
    depositos: [],
    error: null,
    loading: false, 
}

export default function(state = initialState, action){
    switch(action.type){
        case DEPOSIT_LOADING:
            return{
                ...state,
                loading: true,
                error: false
            }
        case DEPOSIT_SUCCESS:
            return{
                loading: false,
                depositos:  action.payload,
                error: false
            }
        case DEPOSIT_ERROR:
            return{
                ...state,
                loading: false,
                error: action.payload
            }
        default:
            return state;
    }
}