import {
    WITHDRAW_SUCCESS,
    WITHDRAW_LOADING,
    WITHDRAW_ERROR
} from '../const'
   
const initialState = {
    retiros: [],
    error: null,
    loading: false, 
}

export default function(state = initialState, action){
    switch(action.type){
        case WITHDRAW_LOADING:
            return{
                ...state,
                loading: true,
                error: false
            }
        case WITHDRAW_SUCCESS:
            return{
                ...state,
                loading: false,
                retiros: action.payload,
                error: false
            }
        case WITHDRAW_ERROR:
            return{
                ...state,
                loading: false,
                error: action.payload
            }
        default:
            return state;
    }
}