import {combineReducers} from 'redux'
import depositosReducer from './depositoReducer'
import swapReducer from './swapReducer'
import withdrawReducer from './withdrawReducer'
import transactionReducer from './transacccionReducer'

export default combineReducers({
    depositos: depositosReducer,
    intercambios: swapReducer,
    retiros: withdrawReducer,
    transacciones: transactionReducer
});