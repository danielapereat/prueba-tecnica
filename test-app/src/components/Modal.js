import React, { useCallback, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { useSpring, animated } from 'react-spring';
import styled from 'styled-components' 
import { MdClose } from 'react-icons/md';
import moment from 'moment';

export const Modal = ({showModal, setShowModal, transactionId, image}) =>{

    const modalRef = useRef();

    const transaccion = useSelector ((state) => state.transacciones.transacciones);
    const {amount, created_at, bought, spent, withdraw_account_id, deposit_provider_id, pair_id, state, to_buy_symbol, to_spend_symbol,
            amount_neto, cost, country, tax, account_from, account_to, action_price} = transaccion.entities.id[transactionId]
    const currency = transaccion.entities.id[transactionId].currency
    const date = moment(created_at).format("MMM-DD-YYYY HH:mm:ss")

    const animation = useSpring({
      config: {
        duration: 250
      },
      opacity: showModal ? 1 : 0,
      transform: showModal ? `translateY(0%)` : `translateY(-100%)`
    });

    const closeModal = e => {
        if (modalRef.current === e.target) {
          setShowModal(false);
        }
      };

    const Background = styled.div`
        width: 100vw;
        height: 100vh;
        background: rgba(0, 0, 0, 0.8);
        position: fixed;
        top: 0px;
        left: 0px;
        display: flex;
        justify-content: center;
        align-items: center;
        @media (max-width: 768px) {
            padding: 2vw;
          }
    
    `
    const ModalBody = styled.div`
        padding: 5.2vw;
        width: 41.6vw;
        height: fit-content;
        box-shadow: 0 5px 16px rgba(0, 0, 0, 0.2);
        background: #fff;
        color: #000;
        display:flex;
        flex-direction: row;
        justify-content: space-evenly;
        position: relative;
        z-index: 10;
        border-radius: 10px;
        @media (max-width: 768px) {
            width: 70vw;
          }
    `

        const CloseModalBtn = styled(MdClose)`
            cursor: pointer;
            position: absolute;
            top: 1vw;
            right: 1vw;
            width: 1.6vw;
            height: 1.6vw;
            padding: 0;
            z-index: 10;
            font-size: 1.5vw;
        `
    const ModalTitle = styled.h3`
        font-family: 'Montserrat', sans-serif;
        font-weight: Bold;
        font-size: 1.8vw;
        margin: 0;
        color: #000000;
        @media (max-width: 768px) {
            font-size: 3vw;
          }
        
    `
    const ModalCategoria = styled.h3`
        font-family: 'Montserrat', sans-serif;
        font-weight: Medium;
        margin: 0.2vw;
        font-size: 1vw;
        color: #666;
        @media (max-width: 768px) {
            font-size: 2vw;
          }
    `
    const Modalinfo = styled.p`
        font-family: 'Montserrat', sans-serif;
        font-weight: light;
        margin: 0.2vw;
        font-size: 0.8vw;
        color: #666;
        @media (max-width: 768px) {
            font-size: 1.8vw;
          }
    `
    const Image = styled.img`
    height: 10vw; 
    `

    return(
        <>
        {showModal ?
        (
            <Background onClick={closeModal} ref={modalRef}>
                 <animated.div style={animation}>
                    <ModalBody showModal={showModal}>
                        {withdraw_account_id  ? (
                                <>
                                <div>
                                <ModalCategoria>Valor de la transacción:</ModalCategoria>
                                    <ModalTitle>{`- $${amount} ${currency.currency.toUpperCase()}`}</ModalTitle>
                                    <Modalinfo>{`Costo de transacción: + $${cost}`}</Modalinfo>
                                    <Modalinfo>{`Costos de impuesto : + $${tax}`}</Modalinfo>
                                    <Modalinfo>{`Valor neto: $${amount_neto}`}</Modalinfo>
                                    <Modalinfo>{`Tipo: ${country}`}</Modalinfo>
                                    <Modalinfo>Retiro</Modalinfo>
                                    <Modalinfo>{date}</Modalinfo>
                                    <ModalCategoria>{state.toUpperCase()}</ModalCategoria>
                                </div>
                                    <Image src={image} alt="Logo de transacción"></Image>
                                </>
                                
                            ):(deposit_provider_id  ? (
                                <>
                                <div>
                                    <ModalCategoria>Valor de la transacción:</ModalCategoria>
                                    <ModalTitle>{`+ $${amount} ${currency.currency.toUpperCase()}`}</ModalTitle>
                                    <Modalinfo>{`Costo de transacción: + $${cost}`}</Modalinfo>
                                    <Modalinfo>{`Valor neto: $${amount_neto}`}</Modalinfo>
                                    <Modalinfo>{`Tipo: ${country}`}</Modalinfo>
                                    <Modalinfo>Deposito</Modalinfo>
                                    <Modalinfo>{date}</Modalinfo>
                                    <ModalCategoria>{state.toUpperCase()}</ModalCategoria>
                                </div>
                                <Image src={image} alt="Logo de transacción"></Image>
                                </>
                            ) : (
                                <>
                                <div>
                                    <ModalCategoria>Valor de la compra:</ModalCategoria>
                                    <ModalTitle>{`+ $${bought} ${to_buy_symbol.toUpperCase()}`}</ModalTitle>
                                    <Modalinfo>{`- $${spent} ${to_spend_symbol.toUpperCase()}`}</Modalinfo>
                                    <Modalinfo>{`Precio de la Acción: ${action_price}`}</Modalinfo>
                                    <Modalinfo>{`Cuenta destinataria: ${account_from}`}</Modalinfo>
                                    <Modalinfo>{`Cuenta destino: ${account_to}`}</Modalinfo>
                                    <Modalinfo>Cambio de moneda</Modalinfo>
                                    <Modalinfo>{date}</Modalinfo>
                                    <ModalCategoria>{state.toUpperCase()}</ModalCategoria>    
                                </div>
                                <Image src={image} alt="Logo de transacción"></Image>
                                </>
                        ))}

                        <CloseModalBtn onClick = {() => setShowModal(prev => !prev)}></CloseModalBtn>

                    </ModalBody>
                 </animated.div>   
            </Background>
        ):(null)
        }
        </>
    )
}