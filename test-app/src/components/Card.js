import { useEffect, useState, React , Fragment} from 'react'
import { useSelector } from 'react-redux'
import moment from 'moment';
import { Modal } from './Modal'
import styled from 'styled-components'
import deposito from '../Images/depositos.gif'
import retiro from '../Images/retiros.gif'
import cambio from '../Images/cambio.gif'



const Card = ({transactionId}) => {

    
    const [showModal, setShowModal] = useState(false);
    const [image, setimage] = useState("");

    const openModal = () =>{
        setShowModal(prev => !prev)
    }

    const transaccion = useSelector ((state) => state.transacciones.transacciones);
    const {amount, created_at, bought, spent, withdraw_account_id, deposit_provider_id, pair_id, state, to_buy_symbol, to_spend_symbol} = transaccion.entities.id[transactionId]
    const currency = transaccion.entities.id[transactionId].currency


    const date = moment(created_at).format("MMM-DD-YYYY HH:mm:ss")

    useEffect(() => {
        if(withdraw_account_id){
            setimage(retiro)
            
        }else if(deposit_provider_id){
            setimage(deposito)
           
        }else{
            setimage(cambio)
    
        }
    
 }, [transactionId]);
 

    const CardInfoContainer = styled.div`
        margin:  2.6vw 5.2vw;
        padding: 2.6vw 5.2vw;
        display: flex;
        min-height: 10.4vw;
        height: fit-content;
        flex-direction: column;
        border-radius: 10px;
        box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.25);
        background: #ffff;
        cursor: pointer;
        background:url(${image}) 0 0 no-repeat;
        background-position: 105% -15%;
        @media (max-width: 768px) {
            background-size: 100px 100px;
            background-position: 108% -15%;
          }
    `

    const CardHeader = styled.div`
        height: 20%;
        flex: display;
        justify-content: flex-start;

    
    `
    const CardBody = styled.div`
        height: 60%;
        font-size: 1.4vw;
        flex: display;
        justify-content: flex-start;
        margin: 0.8vw;

    
    `
    const CardFooter = styled.div`
        height: 20%px;
        font-size: 1.4vw;
        flex: display;
        justify-content: flex-start;

    
    `
    const CardTitle = styled.h3`
        font-family: 'Montserrat', sans-serif;
        font-weight: Bold;
        font-size: 1.8vw;
        margin: 0;
        color: #000000;
        @media (max-width: 768px) {
            font-size: 3vw;
          }
        
    `
    const CardCategoria = styled.h3`
        font-family: 'Montserrat', sans-serif;
        font-weight: Medium;
        margin: 0.2vw;
        font-size: 1vw;
        color: #666;
        @media (max-width: 768px) {
            font-size: 2vw;
          }
    `
    const Cardinfo = styled.p`
        font-family: 'Montserrat', sans-serif;
        font-weight: light;
        margin: 0.2vw;
        font-size: 0.8vw;
        color: #666;
        @media (max-width: 768px) {
            font-size: 1.8vw;
          }
    `

    const ImgCard = styled.img`
        position: relative;
        top: 0;
        right: 10;
        width: 20vw;
    `
  


    return ( 
        <Fragment>
            <div>
                <CardInfoContainer  onClick = {openModal}>
                        {withdraw_account_id  ? (
                            <>
                                <CardHeader>
                                    <CardCategoria>Valor de la transacción</CardCategoria>
                                </CardHeader>
                                <CardBody>
                                    <CardTitle>{`- $${amount} ${currency.currency.toUpperCase()}`}</CardTitle>
                                    <Cardinfo>Retiro</Cardinfo>
                                    <Cardinfo>{date}</Cardinfo>
                                </CardBody>
                                <CardFooter>
                                    <CardCategoria>{state.toUpperCase()}</CardCategoria>
                                </CardFooter>


                            </>
                        ):(deposit_provider_id  ? (
                            <>
                            <CardHeader>
                                <CardCategoria>Valor de la transacción</CardCategoria>
                            </CardHeader>
                            <CardBody>
                                <CardTitle>{`+ $${amount} ${currency.currency.toUpperCase()}`}</CardTitle>
                                <Cardinfo>Deposito</Cardinfo>
                                <Cardinfo>{date}</Cardinfo>
                            </CardBody>
                            <CardFooter>
                                <CardCategoria>{state.toUpperCase()}</CardCategoria>
                            </CardFooter>
                            
                            </>
                        ) : (
                            <>
                            <CardHeader>
                                <CardCategoria>Valor de la compra</CardCategoria>
                                <CardTitle>{`+ $${bought} ${to_buy_symbol.toUpperCase()}`}</CardTitle>
                            </CardHeader>
                            <CardBody>
                                <Cardinfo>{`- $${spent} ${to_spend_symbol.toUpperCase()}`}</Cardinfo>
                                <Cardinfo>Cambio de moneda</Cardinfo>
                                <Cardinfo>{date}</Cardinfo>
                            </CardBody>
                            <CardFooter>
                                <CardCategoria>{state.toUpperCase()}</CardCategoria>
                            </CardFooter>
                            </>
                    ))}
               
                </CardInfoContainer>
            </div>
            <Modal showModal={showModal} setShowModal={setShowModal} transactionId={transactionId} image={image}></Modal>
         </Fragment>
     );
}
 
export default Card;