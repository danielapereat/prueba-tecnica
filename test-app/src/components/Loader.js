import React from 'react'
import imageSrc from '../Images/spinning.gif'
import styled from 'styled-components'

const Loader = props => {

    const LoadingContainer = styled.div`
    width: 80vw;
    display: flex;
    flex-direction: column;
    justify-content: center
`
    return (
        <div>
            <img
            src={imageSrc}
            />
        </div>
    )
}



export default Loader
