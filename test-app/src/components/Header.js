import React from 'react';
import styled from 'styled-components'
import logo from '../Images/logo.ba89d583.png'

const Header = () => {
    const NavBar = styled.div`
        height: 4.1vw;
        display: flex;
        align-items: center;
        justify-content: center;
        background: linear-gradient(#1A85EF, #0E4AC6);
        @media (max-width: 768px) {
            height: 8.1vw;
          }
    `
    const Img = styled.img`
        height: 2.6vw;
        @media (max-width: 768px) {
            height: 4.1vw;
          }
    `
    return ( 
        <NavBar>
            <Img src={logo} alt="Logo de coinsenda"></Img>
        </NavBar>
     );
}
 
export default Header;