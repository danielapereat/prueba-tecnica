import { useEffect, useState, React } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Card from './Card'
import InfiniteScroll from "react-infinite-scroll-component";
import Loader from "../components/Loader";
import styled from 'styled-components'
import { v4 as uuidv4 } from 'uuid';




//Acciones de redux
import { fetchDepositsAction } from '../Actions/depositoActions'
import { fetchWithdrawsAction } from '../Actions/withdrawActions'
import { fetchSwapsAction } from '../Actions/swapActions'
import { fetchTransactionsAction } from '../Actions/transactionActions'







const Content = () => {


    //Styled componenets

    
    const InfoContainer = styled.div`
        display: flex;
        justify-content: center;
        flex-direction: column;
        height: fit-content;
        width: 60vw;
        background-color: #FFFFFF;
        margin: 1vw;
        padding: 2.6vw;
        @media (max-width: 768px) {
            width: 80vw;
          }
    `
    const ContainerHeader = styled.div`
        display: flex;
        justify-content: center;
        height: fit-content;
        background-color: #FFFFFF;
    `
    const Container = styled.div`
        display: flex;
        justify-content: center;
        min-height: 100vh;
        height: fit-content;
        background-color: #EEF0F2;
    `
    const ContentTitle = styled.h3`
        font-family: 'Montserrat', sans-serif;
        font-weight: Bold;
        font-size: 1.8vw;
        margin: 0;
        color: #000000;
    `  
    



    //Crear función con dispatch
    const dispatch = useDispatch();
    const [isDone, setIsDone] = useState(false);
    const [isTransaction, setIsTransaction] = useState(false);
    const [transactionLength, setTransactionLength] = useState(0);
    const [infiniteScrollItems, setInfiniteScrollItems] = useState([]);
    const [hasMore, setHasMore] = useState(true);
    const [isItems, setIisItems] = useState(false);



    //Accerder al state del store
    //const cargando = useSelector ((state) => state.depositos.loading);  
    const depositos = useSelector ((state) => state.depositos.depositos);
    const retiros = useSelector ((state) => state.retiros.retiros);
    const intercambios = useSelector ((state) => state.intercambios.intercambios);



    const fetchDeposit = () => dispatch( fetchDepositsAction() )
    const fetchSwap = () => dispatch( fetchSwapsAction() )
    const fetchWithdraw = () => dispatch( fetchWithdrawsAction() )
    const fetchTransaction = () => dispatch( fetchTransactionsAction(depositos,retiros,intercambios))


    const transacciones = useSelector ((state) => state.transacciones.transacciones);


    useEffect(() => {
        //Consultar la API  
        if(!isDone){
            fetchDeposit() 
            fetchWithdraw() 
            fetchSwap()
            setTimeout(() => {
                setIsDone(true)
              }, 1000);
        }else{
            fetchTransaction()  
            setTimeout(() => {
                setIisItems(true) 
              }, 1000);                                        
        }
          
      }, [isDone])


    useEffect(() => {
        if(isItems){
            setTransactionLength(transactionLength + 10)
        }
        
     }, [isItems]);

     useEffect(() => {
        if(isItems){
            setInfiniteScrollItems(transacciones.result.slice(0,transactionLength))
            setIsTransaction(true)
        }
        
     }, [transactionLength]);


    const fetchMoreData = () => {         
        if(infiniteScrollItems.length < transacciones.result.length){
                setTransactionLength(transactionLength + 10)
                setHasMore(true) 
        }else{
            setHasMore(false)
        }
    }
    


    return ( 
        <Container>
            <InfoContainer>
                        <ContainerHeader>
                            <ContentTitle>Movimientos bancarios</ContentTitle>
                        </ContainerHeader>
                        {isTransaction ? (
                            <InfiniteScroll
                                dataLength={infiniteScrollItems.length}
                                next={() => fetchMoreData()}
                                onscroll={() => fetchMoreData()}
                                hasMore={hasMore}
                                loader={<h2>Cargando...</h2>}  
                                endMessage={
                                    <p style={{ textAlign: "center" }}>
                                      <b>Yay! Haz llegado al final</b>
                                    </p>
                                  }    
                                >
                                {infiniteScrollItems.map(transactionId=> (
                                <Card key={transactionId} transactionId= {transactionId}></Card>
                                ))}
                            </InfiniteScroll>
                        ):(
                            <Loader/>
                        )}
            </InfoContainer>
        </Container>
     );
}
 
export default Content;