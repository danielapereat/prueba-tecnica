import axios from 'axios'
import {
    AUTH_TOKEN,
    USER_ID,
    DEPOSIT_URL,
    WITHDRAW_URL,
    SWAP_URL
   } from '../const'

export const depositsAxios = axios.create({
    baseURL: `${DEPOSIT_URL}users/${USER_ID}/deposits?offset=10&limit=10`,
    headers:{
        Authorization: `Bearer ${AUTH_TOKEN}`,
    }


});

export const swapsAxios = axios.create({
    baseURL: `${SWAP_URL}users/${USER_ID}/swaps`,
    headers:{
        Authorization: `Bearer ${AUTH_TOKEN}`,
    }


});

export const withdrawsAxios = axios.create({
    baseURL: `${WITHDRAW_URL}users/${USER_ID}/withdraws`,
    headers:{
        Authorization: `Bearer ${AUTH_TOKEN}`,
    }


});

// export const withdrawAxios = axios.create({
//     baseURL: `${DEPOSIT_URL}users/${USER_ID}`,
//     headers:{
//         Authorization: `Bearer ${AUTH_TOKEN}`,
//     }
// });

// export const swapAxios = axios.create({
//     baseURL: `${DEPOSIT_URL}users/${USER_ID}`,
//     headers:{
//         Authorization: `Bearer ${AUTH_TOKEN}`,
//     }
// });



