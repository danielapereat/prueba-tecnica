import {
     WITHDRAW_SUCCESS,
     WITHDRAW_ERROR,
     WITHDRAW_LOADING
} from '../const'
import { withdrawsAxios } from '../config/axios'


//Consultar informacion de Depositos

export function fetchWithdrawsAction(){
    return async (dispatch) => {
        dispatch( fetchWithdrawLoading() );
        try {
            const finalResponse = await withdrawsAxios.get() 
            dispatch( fetchWithdrawSucces(finalResponse.data) );
        } catch (error) {
            console.log(error)
            dispatch( fetchWithdrawError(true) );
        }
    }
}


const fetchWithdrawLoading = () => ({
    type:  WITHDRAW_LOADING
})

const fetchWithdrawError = (state) => ({
    type:  WITHDRAW_ERROR,
    payload: state
})

const fetchWithdrawSucces = (finalResponse) => ({
    type:  WITHDRAW_SUCCESS,
    payload: finalResponse
})