import {
    TRANSACTION_LOADING,
    TRANSACTION_SUCCESS,
    TRANSACTION_ERROR
} from '../const'
import { swapsAxios } from '../config/axios'
import { normalize } from 'normalizr';
import { useSelector } from 'react-redux'
import {userListSchema} from '../schema/schema'

//Consultar informacion de Depositos


export function fetchTransactionsAction(depositos, retiros, intercambios){

    return async (dispatch) => {
        dispatch( fetchTransactionsLoading() );
        try {
            const transacionesTemp = depositos.concat(intercambios, retiros)
            const transactionSorted = transacionesTemp.sort((a, b) => new Date(b.created_at) - new Date(a.created_at))

            const transacciones = normalize(transactionSorted, userListSchema )

            //const sortedActivities = 

            
            // transacciones.entities.id.sort(function(a, b) {
            //     var dateA = new Date(a.release), dateB = new Date(b.release);
            //     return dateA - dateB;
            // });
            // console.log("ORIGINAL",transacionesTemp)
            // console.log("SORTED",transactionSorted)


            dispatch( fetchTransactionsSucces(transacciones) );
        } catch (error) {
            console.log(error)
            dispatch( fetchTransactionsError(true) );
        }
    }
}

const fetchTransactionsLoading = () => ({
    type:  TRANSACTION_LOADING
})

const fetchTransactionsError = (state) => ({
    type:  TRANSACTION_ERROR,
    payload: state
})

const fetchTransactionsSucces = (finalResponse) => ({
    type:  TRANSACTION_SUCCESS,
    payload: finalResponse
})