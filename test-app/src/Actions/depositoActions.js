import {
    DEPOSIT_SUCCESS,
    DEPOSIT_ERROR,
    DEPOSIT_LOADING
} from '../const'
import { depositsAxios } from '../config/axios'


//Consultar informacion de Depositos

export function fetchDepositsAction(){
    return async (dispatch) => {
        dispatch( fetchDepositLoading() );
        try {
            const finalResponse = await depositsAxios.get()
            dispatch( fetchDepositSucces(finalResponse.data) );
        } catch (error) {
            console.log(error)
            dispatch( fetchDepositError(true) );
        }
    }
}

const fetchDepositLoading = () => ({
    type: DEPOSIT_LOADING
})

const fetchDepositError = (state) => ({
    type: DEPOSIT_ERROR,
    payload: state
})

const fetchDepositSucces = (finalResponse) => ({
    type: DEPOSIT_SUCCESS,
    payload: finalResponse
})