import {
    SWAP_SUCCESS,
    SWAP_ERROR,
    SWAP_LOADING
} from '../const'
import { swapsAxios } from '../config/axios'



export function fetchSwapsAction(){
    return async (dispatch) => {
        dispatch( fetchSwapLoading() );
        try {
            const finalResponse = await swapsAxios.get()
            dispatch( fetchSwapSucces(finalResponse.data) );
        } catch (error) {
            console.log(error)
            dispatch( fetchSwapError(true) );
        }
    }
}



const fetchSwapLoading = () => ({
    type: SWAP_LOADING
})

const fetchSwapError = (state) => ({
    type: SWAP_ERROR,
    payload: state
})

const fetchSwapSucces = (finalResponse) => ({
    type: SWAP_SUCCESS,
    payload: finalResponse
})